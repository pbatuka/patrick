/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.simplemavenwebapp;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import static org.junit.Assert.*;

/**
 *
 * @author selvy_000
 */
public class TimeAndDateHandlerTest
{
    private LocalDate    theDate;
    private LocalTime    theTime;
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    
    public TimeAndDateHandlerTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
	theDate = LocalDate.now();
	theTime = LocalTime.now();
    }
    
    @After
    public void tearDown()
    {
    }

    /**
     * Test of getDate method, of class TimeAndDateHandler.
     */
    @org.junit.Test
    public void testGetDate()
    {
	System.out.println("getDate");
        TimeAndDateHandler instance = new TimeAndDateHandler();
	String result = instance.getDate().toString();
	String expResult = theDate.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTime method, of class TimeAndDateHandler.
     */
    @org.junit.Test
    public void testGetTime()
    {
        System.out.println("getTime");
	DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        TimeAndDateHandler instance = new TimeAndDateHandler();
	String expResult = dateTimeFormatter.format(theTime);
	String result = instance.getTime();
        assertEquals(expResult, result);
    }
    
}
